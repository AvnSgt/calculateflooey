#include <stdio.h>

int main(void)
{
    /* Symbolic constant "FLOOEY_MULTIPLIER" set to a value of 3.09 */
    const double FLOOEY_MULTIPLIER = 3.09;
    /* Symbolic constant "PROMPT" preset for task. */
    const char PROMPT[24] = {"Please enter a value: \n"};
    /* Value to store user input. Defaults to 0.00. */
    double userInput = 0.00; 
    /* Prototype of flooey function, Requires two values to be passed */
    void flooey(double x, double FLOOEY_MULTIPLIER);

    printf("%s", PROMPT);
    /* %Lf used to collet user input. "Lf" allows for 64 bit doubles, 
    instead of the 32 bit float */
    scanf("%Lf", &userInput);
    /*flooey called, and passed two doubles. */
    flooey(userInput, FLOOEY_MULTIPLIER);    
    
    return 0;
}
/* Declaration of flooey function */
void flooey(double x, double FLOOEY_MULTIPLIER){
    /* Inline variable result for flooey calculation */
    double result = x * FLOOEY_MULTIPLIER;
    /* Print of result as a double with two decimal places. */
    printf("I am a value of: %1.2Lf", result);

}